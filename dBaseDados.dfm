object dtmBaseDados: TdtmBaseDados
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 368
  Top = 293
  Height = 248
  Width = 464
  object BaseDados: TDatabase
    DatabaseName = 'BaseDados'
    DriverName = 'Tibero 6 ODBC Driver'
    LoginPrompt = False
    Params.Strings = (
      'DATABASE NAME=BaseDados'
      'USER NAME=darivaldo_softtek'
      'ODBC DSN=TST1'
      'OPEN MODE=READ/WRITE'
      'SCHEMA CACHE SIZE=8'
      'SQLPASSTHRU MODE=SHARED AUTOCOMMIT'
      'SCHEMA CACHE TIME=-1'
      'MAX ROWS=-1'
      'BATCH COUNT=200'
      'ENABLE SCHEMA CACHE=FALSE'
      'ENABLE BCD=FALSE'
      'ROWSET SIZE=20'
      'BLOBS TO CACHE=64'
      'PASSWORD=funcef01')
    SessionName = 'Default'
    Left = 28
    Top = 24
  end
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Left = 116
    Top = 24
  end
end
