unit fPrincipal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls;

type
  TfmPrincipal = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edNmUsuario: TEdit;
    edSenhaUsuario: TEdit;
    edBaseDados: TEdit;
    btnCorrigeTemplate: TButton;
    lblConectado: TLabel;
    btnQuerys: TButton;
    procedure btnCorrigeTemplateClick(Sender: TObject);
    procedure btnQuerysClick(Sender: TObject);
  private
    function ConectaBD: boolean;
  public

  end;

var
  fmPrincipal: TfmPrincipal;

implementation

uses
  fGeraReport, dBaseDados, fSelects;

{$R *.DFM}

procedure TfmPrincipal.btnCorrigeTemplateClick(Sender: TObject);
begin
  if (ConectaBD) then
    begin
      try
        fmGeraReport:= TfmGeraReport.create(self);
        fmGeraReport.showModal;
      finally
        FreeAndNil(fmGeraReport);
      end;
    end;
end;

function TfmPrincipal.ConectaBD: boolean;
begin
  try
    result:= false;
    lblConectado.Font.Color:= clRed;
    if (edNmUsuario.Text = EmptyStr) then
      begin
        lblConectado.Caption:= 'Informe o nome do usu�rio.';
      end
    else if (edBaseDados.Text = EmptyStr) then
      begin
        lblConectado.Caption:= 'Informe a base de dados.';
      end
    else if (edSenhaUsuario.Text = EmptyStr) then
      begin
        lblConectado.Caption:= 'Informe a senha do usu�rio.';
      end
    else begin
       if dtmBaseDados.ConectaUsuario(edNmUsuario.Text, edSenhaUsuario.Text, edBaseDados.Text) then
         begin
           lblConectado.Font.Color:= clGreen;
           lblConectado.Caption:= 'Conectado';
           result:= dtmBaseDados.BaseDados.connected;
         end;
    end;
    Application.ProcessMessages;
  except on e: exception do
    begin
      lblConectado.Caption :='Falha de conex�o: '+ e.message;
      result:= false;
    end;
  end;
end;

procedure TfmPrincipal.btnQuerysClick(Sender: TObject);
begin
  if (ConectaBD) then
    begin
      try
        fmSelects:= TfmSelects.create(self);
        fmSelects.showModal;
      finally
        FreeAndNil(fmSelects);
      end;
    end;
end;

end.
