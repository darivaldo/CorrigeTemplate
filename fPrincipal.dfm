object fmPrincipal: TfmPrincipal
  Left = 425
  Top = 263
  Width = 529
  Height = 226
  Caption = 'Corre��o de Templates'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 513
    Height = 187
    Align = alClient
    Caption = 'Dados Conex�o'
    TabOrder = 0
    object Label1: TLabel
      Left = 21
      Top = 60
      Width = 36
      Height = 13
      Caption = 'Usu�rio'
    end
    object Label2: TLabel
      Left = 185
      Top = 60
      Width = 31
      Height = 13
      Caption = 'Senha'
    end
    object Label3: TLabel
      Left = 349
      Top = 60
      Width = 73
      Height = 13
      Caption = 'Base de Dados'
    end
    object lblConectado: TLabel
      Left = 2
      Top = 15
      Width = 509
      Height = 13
      Align = alTop
      Alignment = taCenter
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edNmUsuario: TEdit
      Left = 20
      Top = 76
      Width = 157
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      TabOrder = 0
      Text = 'USU�RIO PLANUS'
    end
    object edSenhaUsuario: TEdit
      Left = 184
      Top = 76
      Width = 161
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      PasswordChar = '*'
      TabOrder = 1
      Text = 'FUNCEF01'
    end
    object edBaseDados: TEdit
      Left = 348
      Top = 76
      Width = 121
      Height = 21
      Color = clInfoBk
      TabOrder = 2
      Text = 'TST1'
    end
    object btnCorrigeTemplate: TButton
      Left = 184
      Top = 116
      Width = 161
      Height = 33
      Hint = 'Corrige dados de relat�rios na base de dados'
      Caption = 'Corrige Template'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnCorrigeTemplateClick
    end
    object btnQuerys: TButton
      Left = 352
      Top = 116
      Width = 117
      Height = 33
      Hint = 'Executa querys e comandos DML'
      Caption = 'Querys'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnQuerysClick
    end
  end
end
