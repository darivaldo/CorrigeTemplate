object fmGeraReport: TfmGeraReport
  Left = 324
  Top = 148
  Width = 950
  Height = 427
  HorzScrollBar.Position = 723
  Caption = 'Corre��o de templates de relat�rios manualmente'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object memInserir: TMemo
    Left = -723
    Top = 0
    Width = 1657
    Height = 311
    Align = alClient
    Color = clInfoBk
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = -723
    Top = 311
    Width = 1657
    Height = 60
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 11
      Width = 48
      Height = 13
      Caption = 'IDReports'
    end
    object btnExecutar: TButton
      Left = 496
      Top = 22
      Width = 93
      Height = 26
      Caption = 'Atualizar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btnExecutarClick
    end
    object edIdReports: TEdit
      Left = 33
      Top = 27
      Width = 121
      Height = 21
      TabOrder = 1
      OnExit = edIdReportsExit
    end
    object rgTemplate: TRadioGroup
      Left = 1470
      Top = 15
      Width = 185
      Height = 43
      Align = alRight
      Caption = 'Tabela'
      ItemIndex = 0
      Items.Strings = (
        'Reports')
      TabOrder = 2
    end
    object edNmReports: TEdit
      Left = 156
      Top = 26
      Width = 337
      Height = 21
      Color = clSilver
      ReadOnly = True
      TabOrder = 3
    end
    object btnLocReports: TButton
      Left = 8
      Top = 27
      Width = 25
      Height = 21
      Caption = '...'
      TabOrder = 4
      OnClick = btnLocReportsClick
    end
  end
  object pnlReports: TPanel
    Left = 77
    Top = 36
    Width = 857
    Height = 221
    TabOrder = 2
    object gridBusca: TDBGrid
      Left = 1
      Top = 41
      Width = 855
      Height = 179
      Align = alClient
      DataSource = dsBusca
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = gridBuscaDblClick
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 855
      Height = 40
      Align = alTop
      TabOrder = 1
      object Label2: TLabel
        Left = 16
        Top = 17
        Width = 91
        Height = 13
        Caption = 'Nome do Relat�rio:'
      end
      object edLocalizaPNL: TEdit
        Left = 112
        Top = 13
        Width = 449
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        OnKeyPress = edLocalizaPNLKeyPress
      end
      object btnLocalizaPNL: TButton
        Left = 561
        Top = 12
        Width = 85
        Height = 22
        Caption = 'Localizar'
        TabOrder = 1
        OnClick = btnLocalizaPNLClick
      end
      object lblFecharPNL: TButton
        Left = 826
        Top = 9
        Width = 25
        Height = 25
        Caption = 'X'
        TabOrder = 2
        OnClick = lblFecharPNLClick
      end
    end
  end
  object qry: TQuery
    CachedUpdates = True
    DatabaseName = 'BaseDados'
    UpdateObject = upd
    Left = 204
    Top = 136
  end
  object qryReports: TQuery
    DatabaseName = 'BaseDados'
    Left = 40
    Top = 328
  end
  object qryBusca: TQuery
    DatabaseName = 'BaseDados'
    Left = 841
    Top = 109
  end
  object dsBusca: TDataSource
    DataSet = qryBusca
    Left = 841
    Top = 141
  end
  object upd: TUpdateSQL
    ModifySQL.Strings = (
      'update reports  '
      'set '
      '  template = :template  '
      'where'
      '  idreports = :old_idreports')
    Left = 240
    Top = 136
  end
end
