unit fSelects;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, DBGrids, Db, DBTables, ADODB, ComCtrls,
  Buttons, ImgList;

type
  TfmSelects = class(TForm)
    Qry: TQuery;
    dsQry: TDataSource;
    gridSelect: TDBGrid;
    gbxBotoes: TGroupBox;
    btnQuery: TBitBtn;
    memQuery: TMemo;
    Splitter1: TSplitter;
    QryADO: TADOQuery;
    rgTpQuery: TRadioGroup;
    rgTpComando: TRadioGroup;
    gbxLimite: TGroupBox;
    edLimite: TEdit;
    UpDown1: TUpDown;
    btnCommit: TBitBtn;
    btnRollBack: TBitBtn;
    procedure btnQueryClick(Sender: TObject);
    procedure edLimiteKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure AtlzBotesTransacoes;
    procedure btnCommitClick(Sender: TObject);
    procedure btnRollBackClick(Sender: TObject);
  private
    procedure RollBack;
    procedure Commit;
  public
    { Public declarations }
  end;

var
  fmSelects: TfmSelects;

implementation

uses
  dBaseDados;

{$R *.DFM}


procedure TfmSelects.btnQueryClick(Sender: TObject);
begin
  try
    Screen.Cursor:= crSQLWait;
    if (memQuery.Lines.Count >0) then
      begin
        if (rgTpQuery.ItemIndex = 0) then
          begin
            dsQry.DataSet:= QryADO;

            QryADO.close;
            QryADO.SQL.Clear;
            if (rgTpComando.ItemIndex = 0) then
               QryADO.SQL.Add('SELECT ROWNUM, TBL.* FROM (');

            QryADO.SQL.Add(memQuery.Lines.GetText);

            if (rgTpComando.ItemIndex = 0) then
               QryADO.SQL.Add(')TBL WHERE ROWNUM <= '+ edLimite.Text);

            if (rgTpComando.ItemIndex = 0) then
             begin
                QryADO.Open;
             end
            else begin
              if MessageDlg('Voc� deseja mesmo executar este comando?', mtConfirmation,[mbYes,mbNO],0) = mrYes  then
                begin
                  try
                    if not(dtmBaseDados.ADOConnection.InTransaction) then
                       dtmBaseDados.ADOConnection.BeginTrans;

                    QryADO.ExecSQL;
                  except on e: exception do
                    begin
                      raise exception.create('Ocorreu um erro: '+ e.message);
                      RollBack;
                    end;
                  end;
                end;
            end;
          end
        else begin
            dsQry.DataSet:= Qry;

            Qry.close;
            Qry.SQL.Clear;
            if (rgTpComando.ItemIndex = 0) then
               Qry.SQL.Add('SELECT ROWNUM, TBL.* FROM (');

            Qry.SQL.Add(memQuery.Lines.GetText);

            if (rgTpComando.ItemIndex = 0) then
               Qry.SQL.Add(')TBL WHERE ROWNUM <= '+ edLimite.Text);

            if (rgTpComando.ItemIndex = 0) then
              begin
                 Qry.Open;
              end
            else begin
              if MessageDlg('Voc� deseja mesmo executar este comando?',mtConfirmation,[mbYes,mbNo],0) = mrYes  then
                begin
                  try
                    if not(dtmBaseDados.BaseDados.InTransaction) then
                       dtmBaseDados.BaseDados.StartTransaction;

                    Qry.ExecSQL;
                  except on e: exception do
                    begin
                      raise exception.create('Ocorreu um erro: '+ e.message);
                      RollBack;
                    end;
                  end;
                end;
            end;
        end;

        AtlzBotesTransacoes;
      end
    else MessageDlg('Informe a query de entrada.',mtWarning,[mbOK],0);
  finally
    Screen.Cursor:= crDefault;
  end;
end;

procedure TfmSelects.edLimiteKeyPress(Sender: TObject; var Key: Char);
begin
  if not(key in['0'..'9',#08]) then
     key:= #0;
end;

procedure TfmSelects.FormCreate(Sender: TObject);
begin
  AtlzBotesTransacoes;
end;

procedure TfmSelects.AtlzBotesTransacoes;
begin
  btnRollBack.Enabled:= (dtmBaseDados.BaseDados.InTransaction) or (dtmBaseDados.ADOConnection.InTransaction);
  btnCommit.Enabled  := (dtmBaseDados.BaseDados.InTransaction) or (dtmBaseDados.ADOConnection.InTransaction);
  btnQuery.Enabled   := not(dtmBaseDados.BaseDados.InTransaction) and not(dtmBaseDados.ADOConnection.InTransaction);
  rgTpComando.Enabled:= not(dtmBaseDados.BaseDados.InTransaction) and not(dtmBaseDados.ADOConnection.InTransaction);
  rgTpQuery.Enabled  := not(dtmBaseDados.BaseDados.InTransaction) and not(dtmBaseDados.ADOConnection.InTransaction);

  if (rgTpComando.ItemIndex = 0) then
    btnQuery.Caption:= 'Query'
  else
    btnQuery.Caption:= 'Executar';

  Application.ProcessMessages;
end;

procedure TfmSelects.btnCommitClick(Sender: TObject);
begin
  if MessageDlg('Voc� deseja mesmo fazer commit?', mtConfirmation,[mbYes,mbNO],0) = mrYes  then
     Commit;
end;

procedure TfmSelects.btnRollBackClick(Sender: TObject);
begin
  if MessageDlg('Voc� deseja mesmo fazer rollback?', mtConfirmation,[mbYes,mbNO],0) = mrYes  then
     RollBack;
end;

procedure TfmSelects.Commit;
begin
  if not(dtmBaseDados.BaseDados.InTransaction) then
     dtmBaseDados.BaseDados.Commit;

  if not(dtmBaseDados.ADOConnection.InTransaction) then
     dtmBaseDados.ADOConnection.CommitTrans;

  AtlzBotesTransacoes;
end;

procedure TfmSelects.RollBack;
begin
  if not(dtmBaseDados.BaseDados.InTransaction) then
     dtmBaseDados.BaseDados.Rollback;

  if not(dtmBaseDados.ADOConnection.InTransaction) then
     dtmBaseDados.ADOConnection.RollbackTrans;

  AtlzBotesTransacoes;
end;

end.
