unit dBaseDados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables,uCripto, RipeMD, ADODB;

const
  CONSTCRIPTO = '1';
    
type
  TdtmBaseDados = class(TDataModule)
    BaseDados: TDatabase;
    ADOConnection: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    function conectaWebPlanus(sTNS: String): Boolean;
    function Conectar(sUsuario, sTNS, sSenha : String): Boolean;
    function GetSenhaUsuario(sUsuariobanco, sUsuarioSistema: String) :String;
    function getStringConexaoADO: String;
  public
    procedure CarregaDadosConexao(sUsuario,sTNS,sSenha: String);
    function ConectaUsuario(sUsuario, sSenha, sTNS: String): Boolean;
  end;

var
  dtmBaseDados: TdtmBaseDados;

implementation

{$R *.DFM}

{ TdtmBaseDados }

procedure TdtmBaseDados.CarregaDadosConexao(sUsuario,sTNS,sSenha: String);
begin
  BaseDados.Params.Clear;
  BaseDados.Params.Add('DATABASE NAME=BaseDados');
  BaseDados.Params.Add('USER NAME='+sUsuario);
  BaseDados.Params.Add('ODBC DSN='+sTNS);
  BaseDados.Params.Add('OPEN MODE=READ/WRITE');
  BaseDados.Params.Add('SCHEMA CACHE SIZE=8');
  BaseDados.Params.Add('SQLPASSTHRU MODE=SHARED AUTOCOMMIT');
  BaseDados.Params.Add('SCHEMA CACHE TIME=-1');
  BaseDados.Params.Add('MAX ROWS=-1');
  BaseDados.Params.Add('BATCH COUNT=200');
  BaseDados.Params.Add('ENABLE SCHEMA CACHE=FALSE');
  BaseDados.Params.Add('ENABLE BCD=FALSE');
  BaseDados.Params.Add('ROWSET SIZE=20');
  BaseDados.Params.Add('BLOBS TO CACHE=64');
  BaseDados.Params.Add('PASSWORD='+sSenha);
end;

function TdtmBaseDados.Conectar(sUsuario, sTNS, sSenha : String): Boolean;
begin
   BaseDados.Connected:= false;
   CarregaDadosConexao(sUsuario,sTNS,sSenha);
   BaseDados.Connected:= True;

   result:= BaseDados.Connected;
end;

function TdtmBaseDados.ConectaUsuario(sUsuario, sSenha, sTNS: String): Boolean;
var
  qry: Tquery;
  iIdUsuario: Integer;
  sSenhaBanco,
  sSenhaUsuario: String;
begin
  try
    result:= false;
    if (conectaWebPlanus(sTNS)) then
      begin
         qry:= Tquery.create(self);
         qry.DatabaseName:= dtmBaseDados.BaseDados.Name;
         qry.close;
         qry.sql.text:= 'SELECT SENHA,IDUSUARIO,NOMEUSUARIO FROM USUARIOSISTEMA WHERE NOMEUSUARIO ='+ QuotedStr(Trim(sUsuario));
         qry.open;

         sSenhaBanco:= qry.fieldbyname('SENHA').asstring;
         iIdUsuario := qry.fieldbyname('IDUSUARIO').AsInteger;

         if (sSenhaBanco = CriptografarHash(sSenha, iIdUsuario,15)) then
          begin
             sSenhaUsuario:= GetSenhaUsuario('CM'+ IntToStr(iIdUsuario), sUsuario);
             result       := Conectar('CM'+ IntToStr(iIdUsuario), sTNS, sSenhaUsuario);
          end
         else begin
           MessageDlg('Senha Incorreta', mtError, [mbok], 0);
         end;
      end;
  except on e: exception do
    raise exception.create('Erro ao conectar!'+ #13#10+ e.message);
  end;
end;

function TdtmBaseDados.conectaWebPlanus(sTNS: String): Boolean;
begin
  try
    result:= Conectar('webplanus',sTNS,'webplanus99');
  except
    raise exception.create('Erro ao conectar webplanus!');
  end;
end;

function TdtmBaseDados.GetSenhaUsuario(sUsuariobanco,  sUsuarioSistema: String): String;
var
   Digest :TDigest;
   Crypto :TRipeMD;
begin
   Crypto := TRipeMD.Create(Application);
   Try
     Crypto.Init;
     Crypto.HashString(sUsuariobanco + sUsuarioSistema + CONSTCRIPTO);
     Digest := Crypto.Finish;
     Result := 'C' + Copy(Crypto.GetHashString,1,19);
   finally
     Crypto.Free;
   End;
end;


function TdtmBaseDados.getStringConexaoADO: String;
var
  sLinha,
  sConexao,
  sUser,
  sPassword: String;
  i: Integer;

function DadoCopiado(iLinha: Integer): String;
begin
  result:= Copy(BaseDados.Params[iLinha],pos('=',sLinha) + 1, 500);
end;

begin
  for i:= 0 to BaseDados.Params.Count -1 do
    begin
      sLinha:= UpperCase(BaseDados.Params[i]);
      if pos('ODBC DSN',sLinha) >0 then
        sConexao:= DadoCopiado(i)
      else
      if pos('USER NAME',sLinha) >0 then
        sUser:= DadoCopiado(i)
      else
      if pos('PASSWORD',sLinha) >0 then
        sPassword:= DadoCopiado(i);

      if (sUser<> emptyStr) and (sConexao<> emptyStr)  and (sPassword<> emptyStr) then
          break;
    end;

    result:= 'Provider=tbprov.Tbprov.6;Cache Authentication=False;Encrypt Password=False;Integrated Security="";Mask Password=False;Persist Encrypted=False;' +
             'Persist Security Info=True;Bind Flags=0;Initial Catalog="";Impersonation Level=Anonymous;Location="";Lock Owner="";Mode=ReadWrite;Protection Level=None;' +
             'Extended Properties="";Updatable Cursor=False;Enlist=None;Max Pool Size=100;Min Pool Size=1;Connect Lifetime=0;' +
             'Data Source=' + sConexao + ';Password=' +  sPassword + ';User ID=' + sUser ;
end;

procedure TdtmBaseDados.DataModuleCreate(Sender: TObject);
begin
  ADOConnection.ConnectionString:= getStringConexaoADO;
end;

end.
