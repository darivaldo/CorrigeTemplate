program PrCorrigeTemplate;

uses
  Forms,
  fPrincipal in 'fPrincipal.pas' {fmPrincipal},
  fGeraReport in 'fGeraReport.pas',
  dBaseDados in 'dBaseDados.pas' {dtmBaseDados: TDataModule},
  fSelects in 'fSelects.pas' {fmSelects};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfmPrincipal, fmPrincipal);
  Application.CreateForm(TdtmBaseDados, dtmBaseDados);
  Application.Run;
end.
