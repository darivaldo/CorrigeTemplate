unit fGeraReport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables, ExtCtrls, Grids, DBGrids;

type
  TfmGeraReport = class(TForm)
    memInserir: TMemo;
    qry: TQuery;
    GroupBox1: TGroupBox;
    btnExecutar: TButton;
    edIdReports: TEdit;
    Label1: TLabel;
    rgTemplate: TRadioGroup;
    edNmReports: TEdit;
    btnLocReports: TButton;
    qryReports: TQuery;
    pnlReports: TPanel;
    gridBusca: TDBGrid;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    edLocalizaPNL: TEdit;
    btnLocalizaPNL: TButton;
    lblFecharPNL: TButton;
    qryBusca: TQuery;
    dsBusca: TDataSource;
    upd: TUpdateSQL;
    procedure btnExecutarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLocReportsClick(Sender: TObject);
    procedure lblFecharPNLClick(Sender: TObject);
    procedure btnLocalizaPNLClick(Sender: TObject);
    procedure edIdReportsExit(Sender: TObject);
    procedure gridBuscaDblClick(Sender: TObject);
    procedure edLocalizaPNLKeyPress(Sender: TObject; var Key: Char);
  private
    procedure MostraPanel(bMostra: boolean);
  public
    { Public declarations }
  end;

var
  fmGeraReport: TfmGeraReport;

implementation

uses dBaseDados;

{$R *.DFM}


procedure TfmGeraReport.btnExecutarClick(Sender: TObject);
begin
  try
    Screen.Cursor:= crSQLWait;
    try
      if memInserir.Lines.gettext = EmptyStr then
        begin
          MessageDlg('Informe os dados do SQL.',mtWarning,[mbOK],0);
          exit;
        end;

      if (rgTemplate.ItemIndex <0) then
        begin
           MessageDlg('Selecione a tabela.',mtWarning,[mbOK],0);
            exit;
        end;

      case rgTemplate.ItemIndex of
        0: begin
              if (edIdReports.Text =  EmptyStr) then
                  begin
                     MessageDlg('Informe o ID do relat�rio.',mtWarning,[mbOK],0);
                     exit;
                  end;

               if (qryReports.IsEmpty) then
                 begin
                   MessageDlg('Informe o relat�rio antes de prosseguir.',mtWarning,[mbOK],0);
                   exit;
                 end;

               if MessageDlg('Voc� deseja mesmo continuar com atualiza��o do relat�rio ID:['+ edIdReports.Text +'] Nome:['+edNmReports.Text+'] ?',
                             mtConfirmation, [mbYes,mbNo],0) = mrNo
               then exit;

               if not dtmBaseDados.BaseDados.InTransaction then
                  dtmBaseDados.BaseDados.StartTransaction;

               qry.close;
               qry.sql.text:= 'select * from reports where idreports = ' + qryReports.Fieldbyname('idreports').asstring;
               qry.open;

               qry.Edit;
               qry.FieldByName('template').AsString:= memInserir.Lines.Text;
               qry.Post;

               qry.ApplyUpdates;
            end;
      end;

      if dtmBaseDados.BaseDados.InTransaction then
         dtmBaseDados.BaseDados.Commit;

      MessageDlg('Atualiza��o conclu�da.!',mtInformation,[mbOK],0);
    except on e: exception do
      begin
        if dtmBaseDados.BaseDados.InTransaction then
           dtmBaseDados.BaseDados.Rollback;
        raise exception.create(e.Message);
      end;
    end;
  finally
    Screen.Cursor:= crDefault;
  end;
end;

procedure TfmGeraReport.FormCreate(Sender: TObject);
begin
  MostraPanel(false);
end;

procedure TfmGeraReport.btnLocReportsClick(Sender: TObject);
begin
  MostraPanel(True);
end;

procedure TfmGeraReport.lblFecharPNLClick(Sender: TObject);
begin
  MostraPanel(false);
end;

procedure TfmGeraReport.btnLocalizaPNLClick(Sender: TObject);
begin
  case rgTemplate.ItemIndex of
    0: begin
        if (edLocalizaPNL.Text = EmptyStr) then
          begin
             MessageDlg('Informe o nome do relat�rio.',mtWarning,[mbOK],0);
             exit;
          end;

        qryBusca.close;
        qryBusca.sql.text:= 'select r.idreports, r.name, r.* from reports r where upper(r.name) like '+ QuotedStr(edLocalizaPNL.Text+'%');
        qryBusca.open;
       end;
  end;
end;

procedure TfmGeraReport.edIdReportsExit(Sender: TObject);
begin
  case rgTemplate.ItemIndex of
    0: begin
          if (edIdReports.Text<> EmptyStr) then
            begin
              qryReports.close;
              qryReports.sql.text:= 'select idreports,name,template from reports where idreports = '+ edIdReports.Text;
              qryReports.open;

              memInserir.Lines.Clear;
              if (qryReports.IsEmpty) then
                 edNmReports.Clear
              else begin
                 edNmReports.Text:= qryReports.fieldbyname('name').asstring;
                 //memInserir.Lines.Assign(TBlobField(qryReports.FieldByName('template')));
                 memInserir.Lines.Add(qryReports.FieldByName('template').asstring);
              end;

            end
          else edNmReports.Clear;
       end;
  end;
end;

procedure TfmGeraReport.MostraPanel(bMostra: boolean);
begin
   pnlReports.Left:= (self.Width - pnlReports.Width) div 2;
   pnlReports.Top := (memInserir.top + 40);
   pnlReports.Visible:= bMostra;
     
   if (edLocalizaPNL.CanFocus) then
       edLocalizaPNL.SetFocus;
end;

procedure TfmGeraReport.gridBuscaDblClick(Sender: TObject);
begin
  MostraPanel(False);
  if not(qryBusca.isempty) then
     edIdReports.Text:= qryBusca.fieldbyname('idreports').AsString
  else
     edIdReports.Clear;

  edIdReportsExit(self);
end;

procedure TfmGeraReport.edLocalizaPNLKeyPress(Sender: TObject;
  var Key: Char);
begin
 case key of
    #013: btnLocalizaPNLClick(self);
  end;
end;

end.
