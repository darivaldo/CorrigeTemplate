object fmSelects: TfmSelects
  Left = 241
  Top = 106
  Width = 928
  Height = 613
  Caption = 'Execu��o de selects'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 281
    Width = 912
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object gridSelect: TDBGrid
    Left = 0
    Top = 284
    Width = 912
    Height = 232
    Align = alClient
    DataSource = dsQry
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object gbxBotoes: TGroupBox
    Left = 0
    Top = 516
    Width = 912
    Height = 58
    Align = alBottom
    TabOrder = 1
    object btnQuery: TBitBtn
      Left = 20
      Top = 12
      Width = 120
      Height = 41
      Hint = 'Clique para executar o comando SQL'
      Caption = 'Executar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnQueryClick
    end
    object rgTpQuery: TRadioGroup
      Left = 355
      Top = 8
      Width = 149
      Height = 45
      Caption = 'Tipo Conex�o'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'ADO'
        'BDE')
      TabOrder = 1
    end
    object rgTpComando: TRadioGroup
      Left = 504
      Top = 8
      Width = 153
      Height = 45
      Caption = 'Tipo de Comando'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Open'
        'ExecSQL')
      TabOrder = 2
    end
    object gbxLimite: TGroupBox
      Left = 664
      Top = 8
      Width = 109
      Height = 44
      Caption = 'Limite'
      TabOrder = 3
      object edLimite: TEdit
        Left = 5
        Top = 15
        Width = 77
        Height = 21
        Color = clYellow
        TabOrder = 0
        Text = '100'
        OnKeyPress = edLimiteKeyPress
      end
      object UpDown1: TUpDown
        Left = 82
        Top = 15
        Width = 16
        Height = 21
        Associate = edLimite
        Min = 0
        Position = 100
        TabOrder = 1
        Wrap = False
      end
    end
    object btnCommit: TBitBtn
      Left = 143
      Top = 12
      Width = 100
      Height = 41
      Hint = 'Clique para fazer commit na transa��o'
      Caption = 'Commit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnCommitClick
    end
    object btnRollBack: TBitBtn
      Left = 243
      Top = 12
      Width = 103
      Height = 41
      Hint = 'Clique para fazer rollback na transa��o'
      Caption = 'Rollback'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnRollBackClick
    end
  end
  object memQuery: TMemo
    Left = 0
    Top = 0
    Width = 912
    Height = 281
    Align = alTop
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object Qry: TQuery
    DatabaseName = 'BaseDados'
    Left = 52
    Top = 4
  end
  object dsQry: TDataSource
    DataSet = QryADO
    Left = 88
    Top = 4
  end
  object QryADO: TADOQuery
    Connection = dtmBaseDados.ADOConnection
    Parameters = <>
    Left = 156
    Top = 8
  end
end
